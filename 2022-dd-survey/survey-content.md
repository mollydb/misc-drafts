# Debian developers survey

The Debian contributors behind Freexian would like to get some broad
feedback from all Debian developers on various questions related to
the usage of money in Debian.

The primary goal is to help us set guidelines on how we will use
the money that we would like to inject in the community through our
[Debian Project Funding](https://freexian-team.pages.debian.net/project-funding)
initiative.

However, we hope that the answers to the questions will also help
Debian (and its leaders) identify how to make best use of Debian's
money in general.

We also would like to identify improvement ideas that the Debian
project would like to see come to fruition, and also see where we
could help through targeted application of funds. In short, we want
to know what would be most valuable to fund but also what's
acceptable to be funded as well.

We expect to use Limesurvey (surveys.debian.net) to generate private
links that can be sent to each Debian developer. This is so that only
Debian developers can fill out the survey. Once the survey has concluded,
collated statistics will be shared publicly with the Debian community;
individual responses will naturally be kept private.

## Introductory questions

What kind of contributor do you consider yourself?

* A very regular contributor (contributes almost on a daily basis)
* A regular contributor (contributes on a weekly basis)
* An occasional contributor (contributes on a monthly basis or less)
* An 'emeritus' contributor (very sporadic contributions, used to contribute more)


What are you doing in Debian (please check all that apply) ?

* I'm maintaining packages
* I'm maintaining a service (typically a foo.debian.org or foo.debian.net)
* I maintain Debian-specific software
* I contribute to documentation
* I contribute to translations
* I help organize DebConf
* I help organize local events
* I contribute to mailing list discussions
* I vote on General Resolutions
* I produce merchandise
* Other, please specify: ...


Select the teams to which you regularly contribute:

* Release team
* FTPmasters (archive team)
* System Admistrators ("DSA")
* Security team
* Long Term Support team
* Quality Assurance ("QA")
* Debian Installer
* Debian CD images
* Debian Webmaster
* Debian Publicity
* Debian Community
* Debian Events & Merchandise
* Debian Local Groups
* Debian I18n
* Debian Social
* DebConf related teams
* One (or more) packaging teams
* Other team(s)

What mailing lists are you following?

* debian-devel
* debian-devel-announce
* debian-private
* debian-project
* debian-vote
* debian-mentors
* debian-news


What other media do you use to connect with Debian acquaintances?

* Email
* IRC (`#debian-*` on OFTC)
* IRC (other networks)
* forums.debian.net
* XMPP
* Matrix
* Telegram
* Discord
* Signal
* ...


In what context(s) are you contributing to Debian?

* In my professional life only
* In my personal life only
* In both of them


Is your current Debian involvement level sustainable in the long term?

* Yes
* No
* I don't know


Would you like to spend more time contributing to Debian?

* Yes
* No
* I don't know


## Money and you

Are you currently working a job that pays some or all of your
contributions to Debian?

* Yes, some of my contributions are paid
* Yes, all of my contributions are paid
* No


[ Show the next question only if the person answered "In my personal life
only" to "In what context(s) are you contributing to Debian?" ]

Would you like to get a job where you can contribute to Debian?

* Yes
* No
* I don't know


[ Show the next question only if the person answered "No" to "Is your
current Debian involvement level sustainable in the long term?" ]

Would you be able to sustain your current Debian involvement level in
the long term if some of your Debian work was remunerated?

* Yes
* No
* I don't know


[ Show the next question only if the person answered "Yes" to "Would you
like to spend more time contributing to Debian?" ]

Would you be able to spend more time contributing to Debian if some of
your Debian work was remunerated?

* Yes
* Maybe, but it would require important changes in my life
* No
* I don't know


## Improvement ideas for Debian

Are you aware of the "Grow Your Ideas" project in Debian?
[https://salsa.debian.org/debian/grow-your-ideas](https://salsa.debian.org/debian/grow-your-ideas)


Please rank the ideas listed below in order of most important to
least important. You can put ideas that you find unacceptable below
"further discussion".

* PPA for Debian
* Interactive web interface for bugs.debian.org where you can
  comment and manage bugs
* ...
* Further discussion

Note that most of the ideas have a corresponding issues in the "grow your
ideas" project, and please consider upvoting the ideas there that you
consider worthwhile to pursue. You do that by clicking the "thumbs up"
button at the top of each issue on salsa.debian.org.

*TODO: we must complete this list with concrete ideas collected out of our
own brainstorming and out of a discussion that we should start in debian-devel.*

## Usage of money in Debian

Debian has lots of money on its own, and it can benefit from external
funding on targeted projects as well. Please indicate whether you
agree/disagree with the following assertions:

* As a Debian contributor, I would be fine with having Debian pay other
  Debian contributors
* Debian should experiment with paying Debian contributors to implement
  long-awaited improvements
* Debian should experiment with paying Debian contributors to get rid
  of specific bottlenecks (e.g. NEW processing)
* I am fine with hiring contractors and/or staff who are not already Debian contributors to contribute to Debian.


What kind of work is acceptable to be paid by Debian itself to Debian contributors?

* Work that is limited in scope and not recurring
* Work that is a chore that not enough persons currently want to do
* Work that no volunteer picked up in X months
* Work that is meant to support an existing team of Debian volunteers
* Any work is acceptable to be paid by Debian
* No work is acceptable to be paid by Debian


What kind of work is acceptable to be paid by external funding to Debian contributors?

* Work that is limited in scope and not recurring
* Work that is a chore that not enough persons currently want to do
* Work that no volunteer picked up in X months
* Work that is meant to support an existing team of Debian volunteers
* Any work is acceptable to be paid by external funding
* No work is acceptable to be paid by external funding


On what provisions would you be happy with Debian contributors being paid?

* I have a fair chance to apply for a similar funding
* The paid work is always reviewed/controlled by another volunteer
  Debian contributor
* The paid work is planned and implemented in the open, e.g. using
  Salsa, Debian mailing lists and/or the BTS, and is open to
  contributions from anyone in Debian.
* The paid work complies with the Debian Free Software Guidelines
  (DFSG)
* Where the paid work is packaging, the paid work results in uploads to
  Debian 'main' (i.e. no paid work should benefit to non-free packages)
* The paid work uses tools added to or already in Debian 'main'
* Debian teams may not consist solely of paid contributors
* Paid contributors do not outnumber volunteer contributors in the same
  team


For each possible use of Debian's money quoted below, please indicate if
such a use is a "good idea", "good idea in some specific cases", "bad
idea" or if it's "totally unacceptable". Select "uncertain" if you can't
make up your mind.

* Paying for package maintenance (handling bugs, new upstream release,
  improving packaging, etc.)
* Paying for the initial packaging of software new to Debian.
* Paying for development of new features/improvements for
  Debian-specific infrastructure (e.g. bugs.debian.org,
  tracker.debian.org, dak, etc.)
* Paying development of new features/improvements to Debian specific
  software (e.g. dpkg, apt, debhelper, lintian, etc.)
* Paying development of new tools to experiment new workflows or new
  services
* Use Debian funds to help the Debian Project Leader (DPL) role in some
  way
* Pay Debian contributors to complete large scale changes in a
  reasonable time frame
* Pay specialist porters to support release architectures at risk of
  being dropped from Debian releases due to lack of porters.
* Paying technical writers to improve the documentation for new
  contributors
* Pay Application Managers to ensure we deal with new contributors in a
  timely fashion
* Pay Package Review/Sponsorship when a new contributor has been unable to
  find a sponsor to review his work.
* Paying for debian website translation.
* Hiring administrative and/or support staff (e.g. an operations manager)

Assuming that Debian contributors can request "grants" to help them pursue
some well-defined project, what body should be in charge of
selecting/choosing the grants ?

* The Debian Project Leader (DPL)
* The Debian technical committee
* The Debian developers at large (e.g. through some vote)
* A new team elected for that specific purpose
* A new team designated by the DPL
* The donors who contributed that money
* None of the above

*RH: Maybe this should be a traditional condorcet vote where you rank
acceptable options above "none of the above" by order of preference*


For each role listed below, please answer the following question: Should
this Debian role include a stipend or otherwise be funded to allow more
time to fulfill the obligations of the role? (yes/no/unsure/no opinion)

* Debian Project Leader (DPL)
* Debian Release Manager
    * In general
    * During the freeze
* Member of the archive team ("ftpmasters")
    * In general
    * Those who process NEW and RM
* Member of the Security team
* Member of the LTS team
* Member of the Technical Committee
* Member of the Debian Account Managers


Are there other roles that should be funded to allow more time to fulfill
the obligations of the role?

[Multi-line text field]


Are you concerned that a team of paid contributors could outpace the
ability of Debian volunteers to participate?

* Yes
* Somewhat
* No
* I don't know


Were you aware of the possibility to request a grant from Freexian for
projects improving Debian? It's our [Project
Funding](https://freexian-team.pages.debian.net/project-funding)
initiative.

* Yes
* No


If you have reservations or concerns about paid work that are not included
in the survey, please share them below:

[Multi-line text field]
